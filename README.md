Read all of the settings files, fill out as appropriate before running.

To build: `docker build -t factorio .`


To run: `docker run -p 34197:34197/udp factorio:latest`

Update the map-settings.json, server-settings.json, server-whitelist.json, and map-gen-settings.json as desired. They will be copied into the container on build.