FROM ubuntu

EXPOSE 34197:34197/udp

RUN apt-get update -y
RUN apt-get install wget -y
RUN apt-get install xz-utils

RUN wget -O /tmp/factorio.tar.xz "https://www.factorio.com/get-download/stable/headless/linux64"
RUN ls /tmp
RUN tar -xJf /tmp/factorio.tar.xz -C /opt/

COPY server-settings.json /opt/factorio
COPY map-settings.json /opt/factorio
COPY server-whitelist.json /opt/factorio
COPY map-gen-settings.json /opt/factorio

# Don't run as root
RUN useradd factorio
RUN chown -R factorio:factorio /opt/factorio
RUN su factorio

RUN /opt/factorio/bin/x64/factorio --create /opt/factorio/save-file
ENTRYPOINT /opt/factorio/bin/x64/factorio --start-server /opt/factorio/save-file.zip --server-settings /opt/factorio/server-settings.json
